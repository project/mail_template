<?php
// $Id: mail_template.admin.inc,v 1 vasike Exp $

/**
 * @file
 * Contains administrative pages for creating, editing, and deleting mail templates .
 */

/**
 * Mail Template administration page. Display a list of existing Mail Templates.
 */
function mail_template_admin_page() {
  $mail_templates = mail_template_get_mail_template();
  return theme('mail_template_admin_page', $mail_templates);
}
 
/**
 * Theme the output for the main E-mail Templates administration page.
 */
function theme_mail_template_admin_page($mail_templates) {
  $output = '<p>' . t('This page lists all the <em>E-mail Templates</em> that are currently defined on this system. You may <a href="@add-url">add new templates</a>.', array('@add-url' => url('admin/build/mail_template/add'))) . '</p>';
  
  $destination = drupal_get_destination();
  if (empty($mail_templates)) {
    $rows[] = array(
      array('data' => t('No templates are currently defined.'), 'colspan' => 3),
    );
  }
  else {
    foreach ($mail_templates as $mail_template) {
    $mail_template = mail_template_load($mail_template->name);
    $ops = theme('links', array(
      'mail_template_edit' =>  array('title' => t('edit'), 'href' => "admin/build/mail_template/" . $mail_template->name . '/edit'),
      'mail_template_delete' =>  array('title' => t('delete'), 'href' => "admin/build/mail_template/delete/" . $mail_template->name),
      'mail_template_test' =>  array('title' => t('test'), 'href' => "admin/build/mail_template/" . $mail_template->name . '/test'),
    ));
    
    $rows[] = array(
      l($mail_template->name, 'admin/build/mail_template/' . $mail_template->name),
      $mail_template->title,    
//      $mail_template->object_types,
      $mail_template->default['subject'],
      $ops,
    );
    }
  }
  $header = array(t('Name'), t('Title'), t('Subject'), t('Operations'));
  $output .= theme('table', $header, $rows);
  return $output;
}

/**
 * Add/Edit E-mail Template page.
 */
function mail_template_add_form(&$form_state, $mail_template = NULL) {
  if (!isset($mail_template)) {
    drupal_set_title(t('Add new template'));
  }
  else {
    // Editing an existing template.
    if (empty($mail_template)) {
      drupal_goto('admin/build/mail_template');
    }
    drupal_set_title(t('Edit %title (%name) template', array('%name' => $mail_template->name, '%title' => $mail_template->title)));
  }

  $form['mtid'] = array(
    '#type' => 'value',
    '#value' => $mail_template->mtid,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $mail_template->name,
    '#description' => t('The machine-name for this email template. It may be up to 32 characters long and my only contain lowercase letters, underscores, and numbers. It will be used in URLs and in all API calls.'),
    '#maxlength' => 32,
    '#required' => TRUE,
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $mail_template->title,
    '#description' => t('A short, descriptive title for this email template. It will be used in administrative interfaces, and in page titles and menu items.'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $languages = language_list('enabled');
  $languages = $languages[1];
  foreach ($languages as $language) {
    $langcode = $language->language;
    $mail_template_fields = $mail_template->$langcode;
    $form[$langcode] = array(
    '#type' => 'fieldset',
    '#title' => $language->native.' ('.$language->name.')',
    '#collapsible' => TRUE,
    '#access' => user_access('administer mail template'),
    );
    $form[$langcode]['subject' . $langcode] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => $mail_template_fields['subject'],
    '#description' => t('The subject line of the email template. May includes tokens of any token type specified below.'),
    '#maxlength' => 255,
    '#required' => TRUE,
    );
    
    $form[$langcode]['header' . $langcode.'_format']['header' . $langcode] = array(
    '#type' => 'textarea',
    '#title' => t('Header'),
    '#default_value' => $mail_template_fields['header'],
    '#description' => t('The body of the email template. May includes tokens of any token type specified below.'),
    );
      $form[$langcode]['header' . $langcode]['format'] = filter_form($mail_template_fields['header_format'], NULL, array('header' . $langcode.'_format'));
    
    $form[$langcode]['body' . $langcode.'_format']['body' . $langcode] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => $mail_template_fields['body'],
    '#description' => t('The body of the email template. May includes tokens of any token type specified below.'),
		'#required' => TRUE,
		'#rows' => 20,
    );
      $form[$langcode]['body' . $langcode.'_format']['format'] = filter_form($mail_template_fields['body_format'], NULL, array('body' . $langcode.'_format'));
    
    $form[$langcode]['signature' . $langcode.'_format']['signature' . $langcode] = array(
    '#type' => 'textarea',
    '#title' => t('Signature'),
    '#default_value' => $mail_template_fields['signature'],
    '#description' => t('The body of the email template. May includes tokens of any token type specified below.'),
    );
      $form[$langcode]['signature' . $langcode.'_format']['format'] = filter_form($mail_template_fields['signature_format'], NULL, array('signature' . $langcode.'_format'));
			
    $form[$langcode]['footer' . $langcode.'_format']['footer' . $langcode] = array(
    '#type' => 'textarea',
    '#title' => t('Footer'),
    '#default_value' => $mail_template_fields['footer'],
    '#description' => t('The body of the email template. May includes tokens of any token type specified below.'),
    );
      $form[$langcode]['footer' . $langcode.'_format']['format'] = filter_form($mail_template_fields['footer_format'], NULL, array('footer' . $langcode.'_format'));
  }
/*
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => user_access('administer vr email templates'),
  );
  $form['advanced']['recipient_callback'] = array(
    '#type' => 'textfield',
    '#title' => t('Recipient callback'),
    '#default_value' => $mail_templates->recipient_callback,
    '#description' => t('The name of a function which will be called to retrieve a list of recipients. This function will be called if the query parameter uid=0 is in the URL. This function should return an array of recipients in the form uid|email, as in 136|bob@example.com. If the recipient has no uid, leave it blank but leave the pipe in. Providing the uid allows token substitution for the user.'),
    '#maxlength' => 255,
  );
  $form['advanced']['object_types'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom tokens'),
    '#default_value' => $mail_templates->object_types,
    '#description' => t('List of custom token types this template can handle, one per line. Format is type-name|object-it-acts-on, e.g. my-token-type|user. For types that don\'t require an object, leave the object empty, as in HCI global|. All tokens of type user and node are automatically available to all templates.'),
  );
  $form['token_help'] = array(
    '#title' => t('Mail Template Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Make sure that the tokens you choose are available to your template when in use.'),
  );
  $form['token_help']['help'] = array(
    '#value' => theme('token_help', 'all'),
  );
*/
 $form['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Make sure that the tokens you choose are available to your template when in use.'),
  );

  $form['token_help']['help'] = array(
    '#value' => theme('token_help', 'all'),
  );
/*
  $form['token_help']['mail_template'] = array(
    '#title' => t('E-mail Template patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#description' => t('Make sure that the E-mail Template TOKENs you choose are available to your template when in use.'),
  );
  $form['token_help']['mail_template']['help'] = array(
    '#value' => theme('token_help', 'mail_template_request_args'),
  );
  $form['token_help']['global'] = array(
    '#title' => t('Global patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
//    '#description' => t('Make sure that the tokens you choose are available to your template when in use.'),
  );
  $form['token_help']['global']['help'] = array(
    '#value' => theme('token_help', 'global'),
  );
  $form['token_help']['node'] = array(
    '#title' => t('Content (nodes) patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Make sure that the Node TOKENs you choose are available to your template when in use.'),
  );
  $form['token_help']['node']['help'] = array(
    '#value' => theme('token_help', 'node'),
  );
  $form['token_help']['user'] = array(
    '#title' => t('User patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Make sure that the USER TOKENs you choose are available to your template when in use.'),
  );
  $form['token_help']['user']['help'] = array(
    '#value' => theme('token_help', 'user'),
  );
*/
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Validate the Template.  findme - could do better callback and token type validation
 */
function mail_template_add_form_validate($form, &$form_state) {
  mail_template_validate_name($form_state['values']['name'], $form_state);
}

/**
 * Validate the Template name.
 */
function mail_template_validate_name($name, $form_state) {
  // Ensure a safe machine name.
  if (!preg_match('/^[a-z_][a-z0-9_]*$/', $name)) {
    form_set_error('name', t('The template name may only contain lowercase letters, underscores, and numbers.'));
  }
  
  // Ensure the machine name is unique
  if (empty($form_state['values']['mtid'])) {
    $mail_template = mail_template_load($name);
    if ($mail_template->name == $name) {
      form_set_error('name', t('Template names must be unique. This name is already in use.'));
    }
  }
}
/**
 * Update/create an E-mail Template.
 */
function mail_template_add_form_submit($form, &$form_state) {
  $languages = language_list('enabled');
  $languages = $languages[1];
	//print_r($form_state['values']);
	//exit;
  if (empty($form_state['values']['mtid'])) {
    drupal_write_record('mail_template', $form_state['values']);
		$mail_template = mail_template_load($form_state['values']['name']);
		foreach ($languages as $language) {
			mail_template_save_fields($mail_template, $form_state['values'], $language->language);
		}
    drupal_set_message(t('Template %name has been added.', array('%name' => $form_state['values']['name'])));
    watchdog('mail_template', 'Template %name has been added.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE);
  }
  else {  
    drupal_write_record('mail_template', $form_state['values'], 'mtid');
		$mail_template = mail_template_load($form_state['values']['name']);
		foreach ($languages as $language) {
			mail_template_save_fields($mail_template, $form_state['values'], $language->language);
		}
    drupal_set_message(t('Template %name has been updated.', array('%name' => $form_state['values']['name'])));
    watchdog('mail_template', 'Template %name has been updated.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE);
  }
  
  $form_state['redirect'] = 'admin/build/mail_template';
}

function mail_template_save_fields($mail_template, $values, $langcode) {
  $mail_template_fields = (object)array();
  $mail_template_fields->subject = $values['subject' . $langcode];
  $mail_template_fields->header = $values['header' . $langcode];
  $mail_template_fields->body = $values['body' . $langcode];
  $mail_template_fields->signature = $values['signature' . $langcode];
  $mail_template_fields->footer = $values['footer' . $langcode];
	// formats
  $mail_template_fields->header_format = $values['header' . $langcode.'_format'];
  $mail_template_fields->body_format = $values['body' . $langcode.'_format'];
  $mail_template_fields->signature_format = $values['signature' . $langcode.'_format'];
  $mail_template_fields->footer_format = $values['footer' . $langcode.'_format'];
  $mail_template_fields->language = $langcode;
  $mail_template_fields->mtid = $mail_template->mtid;
  if (empty($mail_template->$langcode)) {
       drupal_write_record('mail_template_fields', $mail_template_fields);
  }
  else {
       drupal_write_record('mail_template_fields', $mail_template_fields, array('mtid', 'language'));
  }
}

/**
 * Delete E-mail Template.
 */
function mail_template_delete_confirm(&$form_state, $name) {
  $mail_templates = mail_template_load($name);
  if (empty($mail_templates)) {
    drupal_goto('admin/build/mail_template');
  }

  $form['name'] = array('#type' => 'value', '#value' => $mail_templates->name);

  return confirm_form(
    $form,
    t('Are you sure you want to delete template %title?', 
    array('%title' => $mail_templates->title)),
    'admin/build/mail_template',
    t('This action cannot be undone.'),
    t('Delete'), 
    t('Cancel')
  );
}

/**
 * Process template delete form submission.
 */
function mail_template_delete_confirm_submit($form, &$form_state) {
  $mail_templates = mail_template_load($form_state['values']['name']);
  
  db_query("DELETE FROM {mail_template} WHERE mtid = %d", $mail_templates->vrmtid);
  db_query("DELETE FROM {mail_template_fields} WHERE mtid = %d", $mail_templates->vrmtid);
  drupal_set_message(t('Template %title has been deleted.', array('%title' => $mail_templates->title)));
  watchdog('mail', 'Template %title has been deleted.', array('%title' => $mail_templates->title), WATCHDOG_NOTICE);

  $form_state['redirect'] = 'admin/build/mail_template';
}

/**
 * Multi-step E-mail Template form.
 */
function mail_template_user_form(&$form_state, $mail_template) {
  $step = empty($form_state['storage']['step']) ? 1 : $form_state['storage']['step'];
  $form_state['storage']['step'] = $step;
  $form_state['storage']['vr_mail_template'] = $mail_template;
  $nid = $form_state['storage']['nid'] = $_REQUEST['nid'];
  $uid = $form_state['storage']['uid'] = $_REQUEST['uid'];
    print_r($form);
  switch ($step) {
    case 1:
    $languages = language_list('enabled');
    $languages = $languages[1];
    foreach ($languages as $language) {
      $options[$language->language] = $language->native;
    }
      $form['language'] = array(
        '#type' => 'select',
        '#title' => t('Select the language'),
    '#options' => $options,
        '#required' => TRUE,
      );
      $form['next'] = array(
        '#type' => 'submit',
        '#value' => t('Next'),
      );
      break;
    case 2:
    $mail_template_fields = $mail_template->$form_state['storage']['language'];
      if (!isset($uid)) {
        $form['mail'] = array(
          '#type' => 'textfield',
          '#title' => t('Recipient\'s e-mail address'),
          '#maxlength' => 255,
          '#required' => TRUE,
        );
      }
      elseif ($uid > 0) {
        $user = user_load(array('uid' => $uid));
        $form['mail'] = array(
          '#type' => 'value',
          '#value' => $user->mail,
        );
        $form['recipients'] = array(
          '#value' => t('Recipient will be ') . $user->mail,
        );
      }
      else {
        $form['recipients'] = array(
          '#value' => t('Recipient list will be generated for preview.'),
        );
      }
      $form['subject'] = array(
        '#type' => 'textfield',
        '#title' => t('Subject'),
        '#maxlength' => 255,
        '#default_value' => $form_state['storage']['subject'] ? $form_state['storage']['subject'] : $mail_template_fields['subject'],
        '#required' => TRUE,
      );
      $form['header'] = array(
        '#type' => 'value',
        '#value' => $form_state['storage']['header'] ? $form_state['storage']['header'] : $mail_template_fields['header'],
      );
      $form['header_show'] = array(
        '#prefix' => '<strong>' . t('Header') . ':</strong>',
        '#value' => $form_state['storage']['header'] ? $form_state['storage']['header'] : $mail_template_fields['header'],
      );
      $form['bodytextarea']['body'] = array(
        '#type' => 'textarea',
        '#title' => t('Body'),
        '#default_value' => $form_state['storage']['body'] ? $form_state['storage']['body'] : $mail_template_fields['body'],
        '#required' => TRUE,
        '#rows' => 10,
        '#description' => t('Review and edit template before previewing. This will not change the template for future emailings, just for this one. To change the template permanently, go to the <a href="@settings">template page</a>. You may use the tokens below.', array('@settings' => url('admin/build/mail_template/edit/' . $mail_templates->name))),
      );
    $form['bodytextarea']['format'] = filter_form('email_templates', NULL, array('bodytextarea'));
      $form['signature'] = array(
        '#type' => 'value',
        '#value' => $form_state['storage']['signature'] ? $form_state['storage']['signature'] : $mail_template_fields['signature'],
      );
      $form['signature1_show'] = array(
        '#prefix' => '<strong>' . t('Signature') . ':</strong>',
        '#value' => $form_state['storage']['signature'] ? $form_state['storage']['signature'] : $mail_template_fields['signature'],
      );
      $form['footer'] = array(
        '#type' => 'value',
        '#value' => $form_state['storage']['footer'] ? $form_state['storage']['footer'] : $mail_template_fields['footer'],
      );
      $form['footer_show'] = array(
        '#prefix' => '<strong>' . t('Footer') . ':</strong>',
        '#value' => $form_state['storage']['footer'] ? $form_state['storage']['footer'] : $mail_template_fields['footer'],
      );
      $form['token_help'] = array(
        '#title' => t('Replacement patterns'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#description' => t('Make sure that the tokens you choose are available to your template when previewed. This list has all tokens. If you use tokens outside the node and user groups, be sure to include them in Custom tokens above. Only tokens accepting node, user, and global objects are currently supported.'),
      );
      $form['token_help']['help'] = array(
        '#value' => theme('token_help', 'all'),
      );
      $form['preview'] = array(
        '#type' => 'submit',
        '#value' => t('Preview'),
      );
      break;

    case 3:
      $form['info'] = array(
        '#value' => t('A preview of the email is shown below. If you\'re satisfied, click Send. If not, click Back to edit the email.'),
      );
      $form['recipients'] = array(
        '#type' => 'textarea',
        '#title' => t('Recipients'),
        '#rows' => 2,
    //'#value' => implode("\n", $form_state['storage']['recipients']),
        '#value' => mail_template_recipients_formatted($form_state['storage']['recipients']),
        '#disabled' => TRUE,
      );
      $form['subject'] = array(
        '#type' => 'textfield',
        '#title' => t('Subject'),
        '#size' => 80,
        '#value' => $form_state['storage']['subject_preview'],
        '#disabled' => TRUE,
      );
     $form['mail_template'] = array(
        '#type' => 'value',
        '#value' => $form_state['storage']['mail_template'],
      );
     $form['mail_template_preview'] = array(
        '#prefix' => '<strong>' . t('E-mail Template') . ':</strong>',
    '#value' => $form_state['storage']['mail_template2'],
      );
     //print_r($form_state);
/*     
      $form['body'] = array(
        '#type' => 'textarea',
        '#title' => t('Body'),
        '#rows' => 15,
        '#value' => $form_state['storage']['body_preview'],
        '#disabled' => TRUE,
      );
      $form['body_label'] = array(
        '#prefix' => '<div class="mail_template_body_label">',
        '#suffix' => '</div>',
        '#value' => '<label>' . t('Body markup:') . '</label>',
      );
      $form['body_preview'] = array(
        '#prefix' => '<div class="mail_template_body_preview">',
        '#suffix' => '</div>',
        '#value' => $form_state['storage']['body_preview'],
      );
*/    
      $form['back'] = array(
        '#type' => 'submit',
        '#value' => t('Back'),
        '#submit' => array('mail_template_user_form_back'),
      );
      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Send email(s)'),
      );
      break;

    case 4:
      drupal_set_message(t('Email(s) sent'));
      unset($form_state['storage']);
      break;
  }
  
  return $form;
}

/**
 * Validate E-mail Template form.
 */
function mail_template_user_form_validate($form, &$form_state) {
  $step = empty($form_state['storage']['step']) ? 1 : $form_state['storage']['step'];

  if ($step == 2) {
    if ($form_state['storage']['uid'] == '0') {
      $recipients = mail_template_recipients($form_state);
      if (!is_array($recipients)) {
        form_set_error('mail_template', t('Callback is not returning an array of recipients.'));
      }
      elseif (count($recipients) < 1) {
        form_set_error('mail_template', t('There are no recipients for this email.'));
      }
    }
    elseif (!valid_email_address($form_state['values']['mail'])) {
      form_set_error('mail', t('You must enter a valid e-mail address.'));
    }
  }
}

/**
 * Form submission.  Take action on step 2 (confirmation of the populated templates).
 */
function mail_template_user_form_submit($form, &$form_state) {  
  $step = empty($form_state['storage']['step']) ? 1 : $form_state['storage']['step'];
  $form_state['storage']['step'] = $step;
  
  switch ($step) {
    case 1:
    $form_state['storage']['language'] = $form_state['values']['language'];
      break;
    case 2:
      $form_state['storage']['mail'] = $form_state['values']['mail'];
      $form_state['storage']['recipients'] = mail_template_recipients($form_state);
    //$form_state['storage']['recipients'] = $form_state['values']['recipients'];
      $form_state['storage']['subject'] = $form_state['values']['subject'];
      //$form_state['storage']['body'] = $form_state['values']['body'];
    $mail_template = '';
    $mail_template .= $form_state['values']['header'];
    $mail_template .= $form_state['values']['body'];
      $mail_template .= $form_state['values']['signature'];
      $mail_template .= $form_state['values']['footer'];
    $form_state['storage']['mail_template'] = $mail_template;
      mail_template_make_preview($form_state);
      break;
      
    case 3:
      $name = $form_state['storage']['mail_template']->name;
      $recipients = $form_state['storage']['recipients'];
      $nid = $form_state['storage']['nid'];
      $params['subject'] = $form_state['storage']['subject'];
      //$body = $form_state['storage']['body'];
    $params['body'] = $form_state['storage']['mail_template'];
    drupal_mail('mail_template', 'test', $form_state['storage']['mail'], $language, $params);
      //mail_template_send_mail($name, $recipients, $nid, $subject, $body);
      break;
  }
  
  $form_state['storage']['step']++;
}

/**
 * Return user to starting point on template multi-form.
 */
function mail_template_user_form_back($form, &$form_state) {
  $form_state['storage']['step'] = 1;
}

/**
 * Generate a preview of the tokenized email for the first in the list.
 */
function mail_template_make_preview(&$form_state) {
  $recipients = $form_state['storage']['recipients'];
  list($uid, $mail) = explode('|', $recipients[0]);
  $subs = mail_template_substitutions($form_state['storage']['vr_mail_template'], $form_state['storage']['nid'], $uid);
  $form_state['storage']['subject_preview'] = token_replace_multiple($form_state['values']['subject'], $subs);
  //$form_state['storage']['body_preview'] = token_replace_multiple($form_state['values']['body'], $subs);
  //$form_state['storage']['mail_template'] = token_replace_multiple($form_state['storage']['mail_template'], $subs);
  $form_state['storage']['mail_template2'] = token_replace_multiple($form_state['storage']['mail_template'], $subs);
}

/**
 * Return an array of email recipients
 */
function mail_template_recipients($form_state) {
  $uid = $form_state['storage']['uid'];
    
  if ($uid == '0') {
    return mail_template_callback_recipients($form_state);
  }
  
  return array($uid . '|' . $form_state['storage']['mail']);
}

/**
 * Return an array of email recipients provided by a callback function
 */
function mail_template_callback_recipients($form_state) {
  $nid = $form_state['storage']['nid'];
  $mail_templates = $form_state['storage']['mail_template'];
  $callback = $mail_templates->recipient_callback;
  $node = empty($nid) ? NULL : node_load($nid); 
  
  if (!empty($callback)) {
    if (function_exists($callback)) {
      return $callback($node);
    }
  }
  
  return array();
}

function mail_template_recipients_formatted($recipients) {
  if (is_array($recipients)) {
    foreach ($recipients as $recipient) {
      list($uid, $mail) = explode('|', $recipient);
      $output .= $mail;
      $output .= $uid ? t(' (user @uid)', array('@uid' => $uid)) : t(' (no user id)');
      $output .= "\n";
    }
    return $output;
  }
}

/**
 * Vienna Residence E-mail Template test recipient callback
 */
function mail_template_test_callback($node = NULL) {
  return array('3|fred@example.com', '7|sally@example.com', '|non.user@example.com');
}