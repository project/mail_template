<?php
// $Id: mail_template.module,v 1 vasike Exp $
/**
 * Action "Send an HTML mail to a user" configuration form.
 */
function mail_template_rules_action_send_mail_form($settings = array(), &$form) {
  $settings += array('from' => '', 'mail_template' => '', 'recipients_type' => '', 'recipients' => array('to' => '', 'roles' => ''), 'ccs' => array('cc' => '', 'bcc' => ''));
  $form['settings']['from'] = array(
    '#type' => 'textfield',
    '#title' => t('Sender'),
    '#default_value' => $settings['from'],
    '#description' => t("The mail's from address. Leave it empty to use the site-wide configured address."),
  );
  $mail_templates = mail_template_get_mail_template();
  foreach ($mail_templates as $mail_template) {
    $options[$mail_template->name] = $mail_template->title.' ('.$mail_template->name.')';
  }
  $form['settings']['mail_template'] = array(
    '#type' => 'select',
    '#title' => t('E-mail Templates'),
    '#options' => $options,
    '#default_value' => isset($settings['mail_template']) ? $settings['mail_template'] : '',
    '#required' => TRUE,
  );
	$recipients_type_options = array(
		'user' => t('to the User defined in the current Rule.'),
		'roles' => t('to Users of a Role(s).'),
		'arbitrary' => t('to arbitrary Mail addresses'),
	);
	 unset($form['#cache']);
	if (empty($settings['recipients_type'])) {
		$form['settings']['recipients_type'] = array(
			'#type' => 'select',
			'#title' => t('What recipients to use for Send Mails'),
			'#default_value' => $settings['recipients_type'],
			'#description' => t("What do you want to use for recipients."),
			'#options' => $recipients_type_options,
			'#required' => TRUE,
		);
		$form['submit']['#submit'] = array('mail_template_rules_action_send_mail_form_step_submit');
		$form['submit']['#value'] = t('Continue');
	} else {
		//print_r($form_state);
		$form['settings']['recipients'] = array(
			'#type' => 'fieldset',
			'#title' => t('Send Mails').' '.$recipients_type_options[$settings['recipients_type']],
			'#required' => TRUE,
		);
		if ($settings['recipients_type']=='arbitrary') {
			$form['settings']['recipients']['to'] = array(
				'#type' => 'textfield',
				'#title' => t('To'),
				'#default_value' => $settings['recipients']['to'],
				'#description' => t("The mail's recipient address. You may separate multiple addresses with comma."),
				'#required' => FALSE,
			);
			$form['settings']['recipients']['roles'] = array('#type' => 'value', '#value' => $settings['recipients']['roles']);
		} elseif ($settings['recipients_type']=='roles') {
			$roles = array_map('filter_xss_admin', user_roles(TRUE));
			$form['settings']['recipients']['roles'] = array(
				'#type' => 'checkboxes',
				'#title' => t('Recipient roles'),
				'#prefix' => t('<strong>WARNING!</strong> This may cause problems if there are too many users of these roles on your site, as your server may not be able to handle all the mail requests all at once.'),
				'#required' => TRUE,
				'#default_value' => isset($settings['recipients']['roles']) ? $settings['recipients']['roles'] : array(),
				'#options' => $roles,
				'#description' => t('Select the roles whose users should receive this mail.'),
			);
			$form['settings']['recipients']['to'] = array('#type' => 'value', '#value' => $settings['recipients']['to']);
		} else {
			$form['settings']['recipients']['to'] = array('#type' => 'value', '#value' => $settings['recipients']['to']);
			$form['settings']['recipients']['roles'] = array('#type' => 'value', '#value' => $settings['recipients']['roles']);
			$form['settings']['recipients']['user']['#value'] = t('To the user defined in the rule.');	
		}
		$form['reset_recipients_type'] = array(
			'#type' => 'checkbox',
			'#title' => t('Reset Recipients Type'),
			'#description' => t("Change the recipient should use (user, roles or arbitrary mail addresses), but keep the other Recipient type settings."),
		);
		$form['#submit'][] = array('mail_template_rules_action_send_mail_form_submit');
	}
	$form['settings']['ccs'] = array(
		'#type' => 'fieldset',
		'#title' => t('CCs, BCCs'),
		'#required' => TRUE,
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	$form['settings']['ccs']['cc'] = array(
		'#type' => 'textfield',
		'#title' => t('CC Recipient'),
		'#default_value' => $settings['cc'],
		'#description' => t("The mail's carbon copy address. You may separate multiple addresses with comma."),
		'#required' => FALSE,
	);
	$form['settings']['ccs']['bcc'] = array(
		'#type' => 'textfield',
		'#title' => t('BCC Recipient'),
		'#default_value' => $settings['bcc'],
		'#description' => t("The mail's blind carbon copy address. You may separate multiple addresses with comma."),
		'#required' => FALSE,
	);
	//print_r($settings);
}

function mail_template_rules_action_send_mail_form_step_submit($form, &$form_state) {
 $form_state['element']['#settings']['recipients_type'] = $form_state['values']['settings']['recipients_type'];
}

function mail_template_rules_action_send_mail_validate($form, &$form_state) {
  if ($form_state['values']['reset_recipients_type']) {
    //Reset the Recipients Type.
		unset($form_state['element']['#settings']['recipients_type']);
		return;
  }
}
/**
 * Action "Send an HTML mail to a user" configuration form.
 */
/*
function mail_template_rules_action_send_mail_eval_form($settings = array(), &$form) {
  mail_template_rules_action_send_mail_form($settings, &$form);
}
*/