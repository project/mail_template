<?php
// $Id: mail_template.module,v 1 vasike Exp $
/**
 * @file
 * Mail Template Rules Integration.
 */
function mail_template_rules_action_info() {
  return array(
    'mail_template_rules_action_send_mail' => array(
      'label' => t('Send Mail Template (User tokens replacement)'),
			'arguments' => array(
				'user' => array('type' => 'user', 'label' => t('User')),
				'node' => array('type' => 'value', 'default value' => NULL),
				'comment' => array('type' => 'value', 'default value' => NULL),
				'term' => array('type' => 'value', 'default value' => NULL),
				//other arguments maybe for language or user or both & maybe others for email function
			),
      'module' => 'Mail Template',
			'base' => 'mail_template_rules_action_send_mail',
			'help' => t('Send Mail Template with user argument. Only global & user tokens are available for replacement in the template.'),
    ),
    'mail_template_rules_action_send_mail_node' => array(
      'label' => t('Send Mail Template (Node & User tokens replacement)'),
			'arguments' => array(
				'user' => array('type' => 'user', 'label' => t('User')),
				'node' => array('type' => 'node', 'label' => t('Content')),
				'comment' => array('type' => 'value', 'default value' => NULL),
				'term' => array('type' => 'value', 'default value' => NULL),
				//other arguments maybe for language or user or both & maybe others for email function
			),
      'module' => 'Mail Template',
			'base' => 'mail_template_rules_action_send_mail',
			'help' => t('Send Mail Template with user & node arguments. Global, node & user tokens are available for replacement in the template.'),
    ),
    'mail_template_rules_action_send_mail_comment' => array(
      'label' => t('Send Mail Template (Comment, Node & User tokens replacement)'),
			'arguments' => array(
				'user' => array('type' => 'user', 'label' => t('User')),
				'node' => array('type' => 'node', 'label' => t('Content')),
				'comment' => array('type' => 'comment', 'label' => t('Comment')),
				'term' => array('type' => 'value', 'default value' => NULL),
				//other arguments maybe for language or user or both & maybe others for email function
			),
//			'eval input' => array('from', 'to', 'cc', 'bcc', 'subject', 'message_html', 'message_plaintext', 'attachments'),
      'module' => 'Mail Template',
			'base' => 'mail_template_rules_action_send_mail',
			'help' => t('Send Mail Template with user, node & comment arguments. Global, node, comment & user tokens are available for replacement in the template.'),
    ),
/*
    'mail_template_rules_action_send_mail_term' => array(
      'label' => t('Send Mail Template (Term & User tokens replacements)'),
			'arguments' => array(
				'user' => array('type' => 'user', 'label' => t('User')),
				'node' => array('type' => 'value', 'default value' => NULL),
				'comment' => array('type' => 'value', 'default value' => NULL),
				'term' => array('type' => 'taxonomy_term', 'label' => t('Term')),
				//other arguments maybe for language or user or both & maybe others for email function
			),
//			'eval input' => array('from', 'to', 'cc', 'bcc', 'subject', 'message_html', 'message_plaintext', 'attachments'),
      'module' => 'Mail Template',
			'base' => 'mail_template_rules_action_send_mail',
			'help' => t('Send Mail Template with user & term arguments. Global, term & user tokens are available for replacement in the template.'),
    ),
*/
/*
    'mail_template_rules_action_send_mail_eval' => array(
      'label' => t('Send Mail within Mail Template (eval)'),
			'eval input' => array('from', 'to', 'cc', 'bcc', 'subject', 'message_html', 'message_plaintext', 'attachments'),
      'module' => 'Mail Template',
    ),
*/
  );
}

//function mail_template_rules_action_send_mail($node = NULL, $user = NULL, $comment = NULL, $term = NULL, $settings) {
function mail_template_rules_action_send_mail() {
	$args = func_get_args();
	//$settings =  mail_template_rules_form_settings_process($args);
	$settings =  call_user_func_array('mail_template_rules_form_settings_process', $args);
	//	mimemail($mail['from'], 'vasike@gmail.com', $mail['subject'], $mail['body'], NULL, array(), $mail['body_plaintext'], $attachments, $mail['key']);
	$status = mimemail($settings['from'], $settings['to'], $settings['subject'], $settings['message_html'], NULL, array(), $settings['message_plaintext'], $attachments, $settings['key']);
  if (!empty($status)) {
    $recipients = array_merge($settings['to'], $settings['bcc'], $settings['cc']);
    watchdog('rules', 'HTML mail successfully sent to %recipient', array('%recipient' => implode(', ', $recipients)));
  }
	//return $settings;
}
/*
function mail_template_rules_action_send_mail_eval($settings) {
	$settings =  mail_template_rules_form_settings_process($settings);
	//	mimemail($mail['from'], 'vasike@gmail.com', $mail['subject'], $mail['body'], NULL, array(), $mail['body_plaintext'], $attachments, $mail['key']);
	$status = mimemail($settings['from'], $settings['to'], $settings['subject'], $settings['message_html'], NULL, array(), $settings['message_plaintext'], $attachments, $settings['key']);
  if (!empty($status)) {
    $recipients = array_merge($settings['to'], $settings['bcc'], $settings['cc']);
    watchdog('rules', 'HTML mail successfully sent to %recipient', array('%recipient' => implode(', ', $recipients)));
  }
	//return $settings;
}
*/
/*
 * Build the mail components: sender, message, to, cc, bcc, attachments
 */
function mail_template_rules_form_settings_process($user = NULL, $node = NULL, $comment = NULL, $term = NULL, $settings) {
  // build to recipients
	$mail = array();
	if ($settings['recipients_type'] == 'roles') {
		$roles = array_filter(array_keys(array_filter($settings['recipients']['roles'])));
		$rids = implode(',', $roles);
		$result = db_query('SELECT DISTINCT u.mail FROM {users} u INNER JOIN {users_roles} r ON u.uid = r.uid WHERE r.rid IN ('. $rids .')');
		while ($account = db_fetch_object($result)) {
      $mail['to'][] = $account->mail;
    }
	} elseif ($settings['recipients_type'] == 'arbitrary') {
		$mail['to'] = empty($settings['recipients']['to']) ? array() : explode(',', $settings['recipients']['to']);
	} else {
		$mail['to'] = $user->mail;
	}
	// We also handle CC and BCC if it's set.
  $mail['cc'] = empty($settings['ccs']['cc']) ? array() : explode(',', $settings['ccs']['cc']);
  $mail['bcc'] = empty($settings['ccs']['bcc']) ? array() : explode(',', $settings['ccs']['bcc']);

  foreach ($mail['to'] as $key => $address) {
    $mail['to'][$key] = str_replace(array("\r", "\n"), '', trim($address));
  }
  foreach ($mail['cc'] as $key => $address) {
    $mail['cc'][$key] = str_replace(array("\r", "\n"), '', trim($address));
  }
  foreach ($mail['bcc'] as $key => $address) {
    $mail['bcc'][$key] = str_replace(array("\r", "\n"), '', trim($address));
  }

  $mail['from'] = str_replace(array("\r", "\n"), '', $settings['from']);

  $attachments = array();
	/*
  $attachments_string = trim($settings['attachments']);
  if (!empty($attachments_string)) {
    $attachment_lines = array_filter(explode("\n", trim($attachments_string)));
    foreach ($attachment_lines as $key => $attachment_line) {
      $attachment = explode(":", trim($attachment_line), 2);
      $attachments[] = array(
        'filepath' => $attachment[1],
        'filemime' => $attachment[0],
      );
    }
  }
	*/
  $mail['attachments'] = empty($attachments[0]['filepath']) ? array() : $attachments;
	
	$mail_temmplate_args = array();
	if (!empty($node)) {
			$mail_temmplate_args['node'] = $node;
	}
	if (!empty($user)) {
			$mail_temmplate_args['user'] = $user;
	}
	if (!empty($comment)) {
			$mail_temmplate_args['comment'] = $comment;
	}
	if (!empty($term)) {
			$mail_temmplate_args['term'] = $term;
	}
	$mail['mail_template'] = mail_template($settings['mail_template'], $langcode = NULL, $mail_temmplate_args);
	$mail['subject'] = str_replace(array("\r", "\n"), '', $mail['mail_template']['subject']);
	$mail['message_html'] = $mail['mail_template']['full_body'];
	$mail['message_plaintext'] = drupal_html_to_text($mail['message_html']);

  return $mail;
}

/**
 * Implementation of hook_rules_condition_info().
 */
/*
function mail_template_rules_condition_info() {
  return array(
    'rules_condition_mail_template' => array(
      'label' => t('Select Mail Template'),
      'arguments' => array(
        'mail_template' => array('mail_template' => 'user', 'label' => t('E-mail Template')),
      ),
      'module' => 'E-mail Templates',
    ),
  );
}
/**
 * Condition: Check for selected content types
 */
/*
function rules_condition_mail_template(&$mail_template, $settings) {
  return $mail_template->name == $settings['mail_template'];
}
*/
/**
* Implementation of hook_rules_data_type_info().
*/
/*
function mail_template_rules_data_type_info() {
  return array(
    'mail_template' => array(
      'label' => t('E-mail Template'),
      'class' => 'rules_data_type_mail_template',
      'savable' => FALSE,
      'identifiable' => TRUE,
    ),
  );
}

/**
* Defines the rules node data type.
*/
/*
class rules_data_type_mail_template extends rules_data_type {

  function load($name) {
    return mail_template_load($name);
  }

  function get_identifier() {
    $mail_template = &$this->get();
    return $mail_template->name;
  }
}
*/